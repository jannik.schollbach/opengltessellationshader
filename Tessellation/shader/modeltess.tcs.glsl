#version 400

layout( vertices = 3 ) out;

in vec4 Position[];
in vec3 Normal[];
in vec3 SNormal[];

out vec4 TCSPosition[];
out vec3 TCSNormal[];

uniform int Inner0;
uniform int Outer0;
uniform int Outer1;
uniform int Outer2;

uniform bool  DepthEnabled;
uniform int   MinTessLevel;
uniform int   MaxTessLevel;
uniform float MaxDepth;
uniform float MinDepth;

uniform mat4  ModelViewMatrix;

void main()
{
    gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;

    TCSPosition[gl_InvocationID] = Position[gl_InvocationID];
    TCSNormal[gl_InvocationID] = Normal[gl_InvocationID];

    if(gl_InvocationID == 0)
    {
        if (DepthEnabled) // Depth based tessellation
        {
            // Position in camera coordinates
            vec4 p = ModelViewMatrix * gl_in[gl_InvocationID].gl_Position;

            // Distance from camera scaled between 0 and 1
            float depth = clamp( (abs(p.z) - MinDepth) / (MaxDepth - MinDepth), 0.0, 1.0 );

            // Tessellation level interpolated between pre-defined min and max level
            float tessLevel = mix(MaxTessLevel, MinTessLevel, depth);

            gl_TessLevelOuter[0] = tessLevel;
            gl_TessLevelOuter[1] = tessLevel;
            gl_TessLevelOuter[2] = tessLevel;

            gl_TessLevelInner[0] = tessLevel;
        }
        else 
        {
            // Uniform tessellation
            gl_TessLevelOuter[0] = float(Outer0);
            gl_TessLevelOuter[1] = float(Outer1);
            gl_TessLevelOuter[2] = float(Outer2);

            gl_TessLevelInner[0] = float(Inner0);
        }
    }
}