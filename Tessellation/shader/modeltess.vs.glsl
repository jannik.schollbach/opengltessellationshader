#version 400

layout (location = 0 ) in vec3 VertexPosition;
layout (location = 1 ) in vec3 aNormal;

out vec4 Position;
out vec3 Normal;

uniform bool TessEnabled;
uniform mat4 MVP;
uniform mat4 ModelViewMatrix;
uniform mat3 NormalMatrix;

void main()
{
    if(TessEnabled)
    {
        Position = vec4(VertexPosition, 1.0);
        Normal = aNormal;
        gl_Position = vec4(VertexPosition, 1.0);
    }
    else
    {
        Position = ModelViewMatrix * vec4(VertexPosition, 1.0);
        Normal = NormalMatrix * aNormal;
        gl_Position = MVP * vec4(VertexPosition, 1.0);
    }
}