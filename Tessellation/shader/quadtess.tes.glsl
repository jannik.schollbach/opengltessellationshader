#version 400

// Specify used primitve, spacing layout and orientation
layout( quads, equal_spacing, ccw ) in;
//layout( quads, fractional_even_spacing, ccw ) in;
//layout( quads, fractional_odd_spacing, ccw ) in;

uniform mat4 MVP;

void main()
{
    // Reads one vertex of 0 <= (u,v) <= 1 created by Tessellator
    float u = gl_TessCoord.x;
    float v = gl_TessCoord.y;

    // 4 given control points
    vec4 p00 = gl_in[0].gl_Position;
    vec4 p10 = gl_in[1].gl_Position;
    vec4 p11 = gl_in[2].gl_Position;
    vec4 p01 = gl_in[3].gl_Position;

    // Linear interpolation from uv to xyz coordinates
    //gl_Position = p00 * (1.0 - u) * (1.0 - v) + p10 * u * (1.0 - v) + p01 * v * (1.0 - u) + p11 * u * v;
    vec4 p1 = mix(p00, p10, u);
    vec4 p2 = mix(p01, p11, u);
    vec4 p = mix(p1, p2, v);

    // Transform to clip coordinates
    gl_Position = MVP * p;
}
