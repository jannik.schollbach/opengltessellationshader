#version 400

// Specify number of used control points
layout( vertices = 16 ) out; // we are tessellating a 4 x 4 Bezier surface quad patch

uniform int Inner0;
uniform int Inner1;
uniform int Outer0;
uniform int Outer1;
uniform int Outer2;
uniform int Outer3;

uniform bool  DepthEnabled;
uniform int   MinTessLevel;
uniform int   MaxTessLevel;
uniform float MaxDepth;
uniform float MinDepth;

uniform mat4  ModelViewMatrix;

void main()
{
     // Pass along the vertex position unmodified
     gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;

     if (DepthEnabled) // Depth based tessellation
     {
        // Position in camera coordinates
        vec4 p = ModelViewMatrix * gl_in[gl_InvocationID].gl_Position;

        // Distance from camera scaled between 0 and 1
        float depth = clamp( (abs(p.z) - MinDepth) / (MaxDepth - MinDepth), 0.0, 1.0 );

        // Tessellation level interpolated between pre-defined min and max level
        float tessLevel = mix(MaxTessLevel, MinTessLevel, depth);

        gl_TessLevelOuter[0] = tessLevel;
        gl_TessLevelOuter[1] = tessLevel;
        gl_TessLevelOuter[2] = tessLevel;
        gl_TessLevelOuter[3] = tessLevel;

        gl_TessLevelInner[0] = tessLevel;
        gl_TessLevelInner[1] = tessLevel;
    }
    else 
    {
        gl_TessLevelOuter[0] = float(Outer0);
        gl_TessLevelOuter[1] = float(Outer1);
        gl_TessLevelOuter[2] = float(Outer2);
        gl_TessLevelOuter[3] = float(Outer3);

        gl_TessLevelInner[0] = float(Inner0);
        gl_TessLevelInner[1] = float(Inner1);
    }
}
