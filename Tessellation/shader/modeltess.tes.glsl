#version 400 core

// Specify used primitve, spacing layout and orientation
layout ( triangles, equal_spacing, ccw ) in;
//layout( triangles, fractional_even_spacing, ccw ) in;
//layout( triangles, fractional_odd_spacing, ccw ) in;

in vec4 TCSPosition[];
in vec3 TCSNormal[];

out vec4 Position;
out vec3 Normal;

uniform mat4 MVP;
uniform mat4 ModelViewMatrix;
uniform mat3 NormalMatrix;


void main(void)
{ 
    // Reads one vertex of 0 <= (u,v,w) <= 1 created by Tessellator
    float u = gl_TessCoord.x;
    float v = gl_TessCoord.y;
    float w = gl_TessCoord.z;

    // Reassign, get all pre-existing control points
    vec3 p0 = gl_in[0].gl_Position.xyz;
    vec3 p1 = gl_in[1].gl_Position.xyz;
    vec3 p2 = gl_in[2].gl_Position.xyz;   

    // Get normals
    vec3 n0 = TCSNormal[0];
    vec3 n1 = TCSNormal[1];
    vec3 n2 = TCSNormal[2]; 

    //Per-Vertex normal with smooth shading --> see https://www.scratchapixel.com/lessons/3d-basic-rendering/introduction-to-shading/shading-normals
    vec3 n = normalize((1 - u - v) * n0 + u * n1 + v * n2);

    // Curved point-normal triangle (10 control points) --> see https://en.wikipedia.org/wiki/Point-normal_triangle
    vec3 w01 = (p1 - p0) * n0;
    vec3 w10 = (p0 - p1) * n1;
    vec3 w12 = (p2 - p1) * n1;
    vec3 w21 = (p1 - p2) * n2;
    vec3 w20 = (p0 - p2) * n2;
    vec3 w02 = (p2 - p0) * n0;

    vec3 p210 = (1.0f / 3.0f) * (2 * p0 + p1 - w01 * n0);
    vec3 p120 = (1.0f / 3.0f) * (2 * p1 + p0 - w10 * n1);
    vec3 p021 = (1.0f / 3.0f) * (2 * p1 + p2 - w12 * n1);
    vec3 p012 = (1.0f / 3.0f) * (2 * p2 + p1 - w21 * n2);
    vec3 p102 = (1.0f / 3.0f) * (2 * p2 + p0 - w20 * n2);
    vec3 p201 = (1.0f / 3.0f) * (2 * p0 + p2 - w02 * n0);

    // Final internal control point
    vec3 e = (1.0f / 6.0f) * (p210 + p120 + p021 + p012 + p102 + p201);
    vec3 vV = (1.0f / 3.0f) * (p0 + p1 + p2);
    vec3 p111 = e + (e - vV) / 2.0f;

    // Construct cubic bezier triangle
    vec3 p = 
    p0 * pow(w, 3) + 
    p1 * pow(u, 3) + 
    p2 * pow(v, 3) +
    3 * p210 * pow(w, 2) * u + 
    3 * p120 * w * pow(u, 2) + 
    3 * p201 * pow(w, 2) * v +
    3 * p021 * pow(u, 2) * v + 
    3 * p102 * w *  pow(v, 2) + 
    3 * p012 * u * pow(v, 2) +
    6 * p111 * w * u * v;

    // Transform to clip coordinates
    gl_Position = MVP * vec4(p, 1.0);

    // Transform to camera coordinates
	Position = ModelViewMatrix * vec4(p, 1.0);
    Normal	=  NormalMatrix * n;
}
