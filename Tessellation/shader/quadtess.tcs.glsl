#version 400

// Specify number of used control points
layout( vertices = 4 ) out;

uniform int Inner0;
uniform int Inner1;
uniform int Outer0;
uniform int Outer1;
uniform int Outer2;
uniform int Outer3;

void main()
{
    // Pass along the vertex position unmodified
    gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;

    // Set inner and outer quad tessellation levels
    gl_TessLevelOuter[0] = float(Outer0);
    gl_TessLevelOuter[1] = float(Outer1);
    gl_TessLevelOuter[2] = float(Outer2);
    gl_TessLevelOuter[3] = float(Outer3);

    gl_TessLevelInner[0] = float(Inner0);
    gl_TessLevelInner[1] = float(Inner1);
}