#version 400 core

// Specify used primitve, spacing layout and orientation
layout ( triangles, equal_spacing, cw ) in;
//layout( triangles, fractional_even_spacing, ccw ) in;
//layout( triangles, fractional_odd_spacing, ccw ) in;

uniform mat4 MVP;

void main(void)
{ 
    // Reads one vertex of 0 <= (u,v,w) <= 1 created by Tessellator
    float u = gl_TessCoord.x;
    float v = gl_TessCoord.y;
    float w = gl_TessCoord.z;

    // 3 given control points
    vec4 p0 = gl_in[0].gl_Position;
    vec4 p1 = gl_in[1].gl_Position;
    vec4 p2 = gl_in[2].gl_Position;

    // Interpolation from uvw to xyz coordinates
    vec4 p = (u * p0 + v * p1 + w * p2);

    // Transform to clip coordinates
	gl_Position = MVP * p;
}