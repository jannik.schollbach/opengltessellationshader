#version 400

layout( isolines ) in; // 2D bezier curve

uniform mat4 MVP;

void main()
{
    // Reads one vertex of 0 <= (u,v) <= 1 created by Tessellator
    float u = gl_TessCoord.x;
    float v = gl_TessCoord.y;

    // 4 given control points
    vec4 p0 = gl_in[0].gl_Position;
    vec4 p1 = gl_in[1].gl_Position;
    vec4 p2 = gl_in[2].gl_Position;
    vec4 p3 = gl_in[3].gl_Position;

    // Bernstein polynomials
    float u1 = (1.0 - u);
    float u2 = u * u;

    float b3 = u2 * u;
    float b2 = 3.0 * u2 * u1;
    float b1 = 3.0 * u * u1 * u1;
    float b0 = u1 * u1 * u1;

    // Cubic Bezier interpolation (Bezier curve)
    vec4 p = p0 * b0 + p1 * b1 + p2 * b2 + p3 * b3;

    // Shifting line based on v coordinate
    float shiftFactor = v * 0.5f;

    // Transform to clip coordinates
    gl_Position = MVP * (p + shiftFactor);

}
