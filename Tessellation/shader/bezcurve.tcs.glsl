#version 400

// Specify number of used control points
layout( vertices = 4 ) out;

uniform int NumStrips;
uniform int NumSegments;

void main()
{
    // Pass along the vertex position unmodified
    gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;

    // Some drivers (e.g. Intel) treat these levels incorrectly.  The OpenGL spec
    // says that level 0 should be the number of strips and level 1 should be
    // the number of segments per strip.  Unfortunately, not all drivers do this.
    // If this example doesn't work for you, try switching the right
    // hand side of the two assignments below.
    gl_TessLevelOuter[0] = float(NumStrips);    // specifies how many isolines to create, this becomes the maximum value for gl_TessCoord.y in the TES
    gl_TessLevelOuter[1] = float(NumSegments);  // specifies how many times to split up a particular line, this how far apart gl_TessCoord.x is in different invocations
}
