#version 400

noperspective in vec3 EdgeDistance;
in vec3 GSNormal;
in vec4 GSPosition;

layout ( location = 0 ) out vec4 FragColor;

uniform vec3  LightDirection;
uniform vec3  Ka;
uniform vec3  Kd;
uniform vec3  Ks;
uniform float SpecExp;

uniform bool  WireEnabled;
uniform float LineWidth;
uniform vec4  LineColor;

vec3 blinnPhong(vec3 lightVector, vec3 viewVector, vec3 normal, vec3 ambientColor, vec3 diffuseColor, vec3 specularColor, float specularExponent)
{
    // Lighting based on the blinn-phong lighting model
	float nDotl = max(dot(normal, lightVector), 0);
	vec3 halfwayVector = normalize(lightVector + viewVector);
	float nDoth = max(dot(normal, halfwayVector), 0);
	return ambientColor + nDotl * diffuseColor + specularColor * pow(nDoth, specularExponent);
}

float edgeMix()
{
    // Find the smallest distance
    float d = min( min( EdgeDistance.x, EdgeDistance.y ), EdgeDistance.z );

    if( d < LineWidth - 1 ) {
        return 1.0;
    } else if( d > LineWidth + 1 ) {
        return 0.0;
    } else {
        float x = d - (LineWidth - 1);
        return exp2(-2.0 * (x*x));
    }
}

void main()
{
    float mixVal = edgeMix();

    vec3 viewPosition = vec3(0, 0, 0);
	vec3 lightVector = LightDirection;
	vec3 viewVector = normalize(viewPosition - GSPosition.xyz);
    vec3 normalVector = normalize(GSNormal);

    if(dot(normalVector, viewVector) < 0)
        normalVector = -normalVector;

	vec3 lighting = blinnPhong(lightVector, viewVector, normalVector, Ka, Kd, Ks, SpecExp);
    vec4 outColor = vec4(lighting, 1.0);

    if(WireEnabled)
        FragColor = mix(outColor, LineColor, mixVal); 
    else
        FragColor = outColor;	
}