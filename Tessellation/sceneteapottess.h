#ifndef SCENETESSTEAPOTDEPTH_H
#define SCENETESSTEAPOTDEPTH_H

#include "scene.h"
#include "glslprogram.h"
#include "teapotpatch.h"

#include "cookbookogl.h"

#include <glm/glm.hpp>

class SceneTeapotTess : public Scene
{
private:
    GLSLProgram prog;

    GLuint vaoHandle;

    TeapotPatch teapot;

    glm::mat4 viewport;

    glm::vec3 translationVec;

    void setMatrices();
    void compileAndLinkShader();

    /**
      Tessellation parameters.
      */
    struct
    {
        int tessInner0 = 4;
        int tessInner1 = 4;
        int tessOuter0 = 4;
        int tessOuter1 = 4;
        int tessOuter2 = 4;
        int tessOuter3 = 4;

        bool    wireEnabled = true;
        bool    depthEnabled = false;
        int     minTessLevel = 2;
        int     maxTessLevel = 15;
        float   maxDepth = 20.0f;
        float   minDepth = 2.0f;

        double   frameTime = 0.0f;
    } m_uiData;

    double toSecond = 0.0f;
    double lastTime = 0.0f;

public:
    SceneTeapotTess();

    void initScene();
    void update( float t );
    void render();
    void renderGUI();
    void resize(int, int);
    void translationUpdate(glm::vec3 transVec);
};

#endif // SCENETESSTEAPOTDEPTH_H
