#include "scenerunner.h"
#include "sceneprimitivetess.h"
#include "sceneteapottess.h"
#include "scenemodeltess.h"

#include <map>

std::map<std::string, std::string> sceneInfo = 
{
            { "tess-basics", "Demonstrates how tessellation works on different primitives" },
            { "tess-teapot", "Uses tessellation to draw teapots, varies the amount of tessellation with depth" },
            { "tess-model", "Loads a model mesh via assimp to demonstrate tessellation on a 3D triangle mesh" },
};

int main(int argc, char* argv[])
{
    std::string recipe = SceneRunner::parseCLArgs(argc, argv, sceneInfo);

    SceneRunner runner("Tessellation Shader - " + recipe);

    std::unique_ptr<Scene> scene;
    if (recipe == "tess-basics") {
    scene = std::unique_ptr<Scene>(new ScenePrimitiveTess());
    }
    else if (recipe == "tess-teapot") {
        scene = std::unique_ptr<Scene>(new SceneTeapotTess());
    }    
    else if (recipe == "tess-model") {
        scene = std::unique_ptr<Scene>(new SceneModelTess());
    }
    else {
        printf("Unknown recipe: %s\n", recipe.c_str());
        exit(EXIT_FAILURE);
    }

    return runner.run(std::move(scene));
}
