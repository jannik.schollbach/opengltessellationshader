#pragma once

#include "trianglemeshloader.h"

#include "scene.h"
#include "glslprogram.h"

class SceneModelTess : public Scene
{
private:
    GLSLProgram prog; 
    GLSLProgram progTess; // Shader program for rendering with tessellation shaders

    GLuint vaoHandle;

    TriangleMeshLoader* loadedModel = new TriangleMeshLoader("./resources/egyptian.obj");

    glm::mat4 viewport;

    glm::vec3 translationVec;

    void setUniforms(GLSLProgram& prog);
    void compileAndLinkShader();

    /**
      Tessellation parameters.
      */
    struct
    {
        int tessInner0 = 10;
        int tessOuter0 = 10;
        int tessOuter1 = 10;
        int tessOuter2 = 10;

        bool    wireEnabled = true;
        bool    tessEnabled = false;
        bool    depthEnabled = false;
        int     minTessLevel = 2;
        int     maxTessLevel = 15;
        float   maxDepth = 20.0f;
        float   minDepth = 2.0f;

        double   frameTime = 0.0f;
    } m_uiData;

    double toSecond = 0.0f;
    double lastTime = 0.0f;

public:
    SceneModelTess();

    void initScene();
    void update(float t);
    void render();
    void renderGUI();
    void resize(int, int);
    void translationUpdate(glm::vec3 transVec);
};
