#include "sceneprimitivetess.h"

#include <iostream>
#include <vector>
using std::cerr;
using std::endl;

#include "glutils.h"

#include <glm/gtc/matrix_transform.hpp>
using glm::vec3;
using glm::mat4;
using glm::vec4;

ScenePrimitiveTess::ScenePrimitiveTess() { }

void ScenePrimitiveTess::initScene()
{
    compileAndLinkShader();

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    model = mat4(1.0f);
    float c = 3.5f;
    projection = glm::ortho(-0.4f * c, 0.4f * c, -0.3f *c, 0.3f*c, 0.1f, 100.0f);

    // Initialize different patch primitives
    initIsoline();
    initTriangle();
    initQuad();

    // Maximum of supported control points per patch, you get an error if your number of defined GL_PATCH_VERTICES is greater
    GLint maxVerts;
    glGetIntegerv(GL_MAX_PATCH_VERTICES, &maxVerts);
    std::cout << "Max patch vertices: " << maxVerts << std::endl;
}


void ScenePrimitiveTess::initIsoline()
{
    glEnable(GL_DEPTH_TEST);
    glPointSize(10.0f);

    // Set up a line primitive based on a bezier curve with 4 control points
    std::vector<glm::f32vec2> bezierVertices;
    bezierVertices.emplace_back(-1.0f, -1.0f);
    bezierVertices.emplace_back(-0.5f, 1.0f);
    bezierVertices.emplace_back(0.5f, -1.0f);
    bezierVertices.emplace_back(1.0f, 1.0f);

    // Set up patch VBO
    GLuint vboHandleIsoline;
    glGenBuffers(1, &vboHandleIsoline);

    glBindBuffer(GL_ARRAY_BUFFER, vboHandleIsoline);
    glBufferData(GL_ARRAY_BUFFER, bezierVertices.size() * sizeof(glm::f32vec2), bezierVertices.data(), GL_STATIC_DRAW);

    // Set up patch VAO
    glGenVertexArrays(1, &vaoHandleIsoline);
    glBindVertexArray(vaoHandleIsoline);

    glBindBuffer(GL_ARRAY_BUFFER, vaoHandleIsoline);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(0);

    glBindVertexArray(0);
}

void ScenePrimitiveTess::initTriangle()
{
    // Set up a triangle primitive with 3 control points
    std::vector<glm::f32vec2> triangleVertices;
    triangleVertices.emplace_back(-0.5f, -0.5f);
    triangleVertices.emplace_back(0.0f, 0.5f);
    triangleVertices.emplace_back(0.5f, -0.5f);

    // Set up patch VBO
    GLuint vboHandleTriangle;
    glGenBuffers(1, &vboHandleTriangle);

    glBindBuffer(GL_ARRAY_BUFFER, vboHandleTriangle);
    glBufferData(GL_ARRAY_BUFFER, triangleVertices.size() * sizeof(glm::f32vec2), triangleVertices.data(), GL_STATIC_DRAW);

    // Set up patch VAO
    glGenVertexArrays(1, &vaoHandleTriangle);
    glBindVertexArray(vaoHandleTriangle);

    glBindBuffer(GL_ARRAY_BUFFER, vaoHandleTriangle);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(0);

    glBindVertexArray(0);
}

void ScenePrimitiveTess::initQuad()
{
    // Set up a quad primitive with 4 control points
    std::vector<glm::f32vec2> quadVertices;
    quadVertices.emplace_back(-0.5f, -0.5f);
    quadVertices.emplace_back(0.5f, -0.5f);
    quadVertices.emplace_back(0.5f, 0.5f);
    quadVertices.emplace_back(-0.5f, 0.5f);

    // Set up patch VBO
    GLuint vboHandleQuad;
    glGenBuffers(1, &vboHandleQuad);

    glBindBuffer(GL_ARRAY_BUFFER, vboHandleQuad);
    glBufferData(GL_ARRAY_BUFFER, quadVertices.size() * sizeof(glm::f32vec2), quadVertices.data(), GL_STATIC_DRAW);

    // Set up patch VAO
    glGenVertexArrays(1, &vaoHandleQuad);
    glBindVertexArray(vaoHandleQuad);

    glBindBuffer(GL_ARRAY_BUFFER, vboHandleQuad);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(0);

    glBindVertexArray(0);
}

void ScenePrimitiveTess::update(float t) 
{
    // Update frame time every 0.5 seconds
    if (t - toSecond >= 0.5f)
    {
        m_uiData.frameTime = (t - lastTime) * 1000;
        toSecond = t;
    }

    lastTime = t;
}

void ScenePrimitiveTess::render()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    vec3 cameraPos(0.0f,0.0f,1.5f);
    view = glm::lookAt(cameraPos, vec3(0.0f,0.0f,0.0f), vec3(0.0f,1.0f,0.0f));

    if(m_uiData.useTessIsoline && !m_uiData.useTessTriangle && !m_uiData.useTessQuad)
    {
        progIsoline.use();
        setMatrices(progIsoline);

        ///////////// Uniforms ////////////////////
        progIsoline.setUniform("NumSegments", m_uiData.numSegments);
        progIsoline.setUniform("NumStrips", m_uiData.numStrips);
        progIsoline.setUniform("LineColor", vec4(1.0f, 0.0f, 0.0f, 1.0f));
        /////////////////////////////////////////////

        // Set the number of control points per patch. IMPORTANT!
        glPatchParameteri(GL_PATCH_VERTICES, 4);

        // Draw the curve
        glBindVertexArray(vaoHandleIsoline);
        glDrawArrays(GL_PATCHES, 0, 4);

        // Draw the control points of bezier curve
        progSolid.use();
        setMatrices(progSolid);

        glDrawArrays(GL_POINTS, 0, 4);
    }
    else if(m_uiData.useTessTriangle && !m_uiData.useTessIsoline && !m_uiData.useTessQuad)
    {
        progTriangle.use();
        setMatrices(progTriangle);

        ///////////// Uniforms ////////////////////
        progTriangle.setUniform("Inner0", m_uiData.tessInner0);
        progTriangle.setUniform("Outer0", m_uiData.tessOuter0);
        progTriangle.setUniform("Outer1", m_uiData.tessOuter1);
        progTriangle.setUniform("Outer2", m_uiData.tessOuter2);
        progTriangle.setUniform("LineWidth", 1.5f);
        progTriangle.setUniform("LineColor", vec4(1.0f, 1.0f, 1.0f, 1.0f));
        progTriangle.setUniform("TriangleColor", vec4(1.0f, 0.0f, 0.0f, 1.0f));
        /////////////////////////////////////////////

        // Set the number of control points per patch. IMPORTANT!
        glPatchParameteri(GL_PATCH_VERTICES, 3);

        // Draw the triangle
        glBindVertexArray(vaoHandleTriangle);
        glDrawArrays(GL_PATCHES, 0, 3);
    }
    else if(m_uiData.useTessQuad && !m_uiData.useTessIsoline && !m_uiData.useTessTriangle)
    {
        progQuad.use();
        setMatrices(progQuad);

        ///////////// Uniforms ////////////////////
        progQuad.setUniform("Inner0", m_uiData.tessInner0);
        progQuad.setUniform("Inner1", m_uiData.tessInner1);
        progQuad.setUniform("Outer0", m_uiData.tessOuter0);
        progQuad.setUniform("Outer1", m_uiData.tessOuter1);
        progQuad.setUniform("Outer2", m_uiData.tessOuter2);
        progQuad.setUniform("Outer3", m_uiData.tessOuter3);
        progQuad.setUniform("LineWidth", 1.5f);
        progQuad.setUniform("LineColor", vec4(1.0f, 1.0f, 1.0f, 1.0f));
        progQuad.setUniform("QuadColor", vec4(1.0f, 0.0f, 0.0f, 1.0f));
        /////////////////////////////////////////////

        // Set the number of control points per patch. IMPORTANT!
        glPatchParameteri(GL_PATCH_VERTICES, 4);

        // Draw the quad
        glBindVertexArray(vaoHandleQuad);
        glDrawArrays(GL_PATCHES, 0, 4);
    }
    else 
    {
        // Draw nothing
    }

    glFinish();
}

void ScenePrimitiveTess::renderGUI()
{
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();

    ImGui::Begin("Options - Tessellation Basics");
    ImGui::Spacing();
    ImGui::Text("FrameTime [ms]: %.5f", m_uiData.frameTime);
    ImGui::Text("Frames    [s]: %i", int(1000.0f / m_uiData.frameTime));
    ImGui::Spacing();
    ImGui::Text("Choose Tessellation Primitive:");
    ImGui::Checkbox("Line", &m_uiData.useTessIsoline);
    ImGui::Checkbox("Triangle", &m_uiData.useTessTriangle);
    ImGui::Checkbox("Quad", &m_uiData.useTessQuad);
    ImGui::Spacing();
    if (ImGui::CollapsingHeader("Line Tessellation"))
    {
        ImGui::SliderInt("Strips   Outer [0]", &m_uiData.numStrips, 1, 20);
        ImGui::SliderInt("Segments Outer [1]", &m_uiData.numSegments, 1, 50);
    }
    ImGui::Spacing();
    if (ImGui::CollapsingHeader("Triangle Tessellation"))
    {
        ImGui::SliderInt("Inner Level[0]", &m_uiData.tessInner0, 1, 64);
        ImGui::SliderInt("Outer Level[0]", &m_uiData.tessOuter0, 1, 64);
        ImGui::SliderInt("Outer Level[1]", &m_uiData.tessOuter1, 1, 64);
        ImGui::SliderInt("Outer Level[2]", &m_uiData.tessOuter2, 1, 64);
    }
    ImGui::Spacing();
    if (ImGui::CollapsingHeader("Quad Tessellation"))
    {
        ImGui::SliderInt("Inner Level[0]", &m_uiData.tessInner0, 1, 64);
        ImGui::SliderInt("Inner Level[1]", &m_uiData.tessInner1, 1, 64);
        ImGui::SliderInt("Outer Level[0]", &m_uiData.tessOuter0, 1, 64);
        ImGui::SliderInt("Outer Level[1]", &m_uiData.tessOuter1, 1, 64);
        ImGui::SliderInt("Outer Level[2]", &m_uiData.tessOuter2, 1, 64);
        ImGui::SliderInt("Outer Level[3]", &m_uiData.tessOuter3, 1, 64);
    }

    ImGui::End();
    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}

void ScenePrimitiveTess::setMatrices(GLSLProgram& prog)
{
    mat4 mv = view * model;
    prog.setUniform("MVP", projection * mv);
    prog.setUniform("ViewportMatrix", viewport);
}

void ScenePrimitiveTess::resize(int w, int h)
{
    glViewport(0,0,w,h);

    float w2 = w / 2.0f;
    float h2 = h / 2.0f;
    viewport = mat4( vec4(w2,0.0f,0.0f,0.0f),
                     vec4(0.0f,h2,0.0f,0.0f),
                     vec4(0.0f,0.0f,1.0f,0.0f),
                     vec4(w2+0, h2+0, 0.0f, 1.0f));
}

void ScenePrimitiveTess::translationUpdate(glm::vec3 transVec) {}

void ScenePrimitiveTess::compileAndLinkShader()
{
	try {
        progIsoline.compileShader("shader/bezcurve.vs.glsl", GLSLShader::VERTEX);
        progIsoline.compileShader("shader/bezcurve.fs.glsl", GLSLShader::FRAGMENT);
        progIsoline.compileShader("shader/bezcurve.tes.glsl", GLSLShader::TESS_EVALUATION);
        progIsoline.compileShader("shader/bezcurve.tcs.glsl", GLSLShader::TESS_CONTROL);
        progIsoline.link();

        progSolid.compileShader("shader/solid.vs.glsl", GLSLShader::VERTEX);
        progSolid.compileShader("shader/solid.fs.glsl", GLSLShader::FRAGMENT);
        progSolid.link();

        progTriangle.compileShader("shader/triangletess.vs.glsl", GLSLShader::VERTEX);
        progTriangle.compileShader("shader/triangletess.fs.glsl", GLSLShader::FRAGMENT);
        progTriangle.compileShader("shader/triangletess.gs.glsl", GLSLShader::GEOMETRY);
        progTriangle.compileShader("shader/triangletess.tes.glsl", GLSLShader::TESS_EVALUATION);
        progTriangle.compileShader("shader/triangletess.tcs.glsl", GLSLShader::TESS_CONTROL);
        progTriangle.link();

		progQuad.compileShader("shader/quadtess.vs.glsl", GLSLShader::VERTEX);
        progQuad.compileShader("shader/quadtess.fs.glsl", GLSLShader::FRAGMENT);
        progQuad.compileShader("shader/quadtess.gs.glsl", GLSLShader::GEOMETRY);
        progQuad.compileShader("shader/quadtess.tes.glsl", GLSLShader::TESS_EVALUATION);
        progQuad.compileShader("shader/quadtess.tcs.glsl", GLSLShader::TESS_CONTROL);
        progQuad.link();
    } catch(GLSLProgramException &e ) {
    	cerr << e.what() << endl;
 		exit( EXIT_FAILURE );
    }
}
