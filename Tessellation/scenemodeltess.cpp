#include "scenemodeltess.h"

#include <iostream>
using std::cerr;
using std::endl;

#include <glm/gtc/matrix_transform.hpp>
using glm::vec3;
using glm::vec4;
using glm::mat4;
using glm::mat3;

SceneModelTess::SceneModelTess() {}

void SceneModelTess::initScene()
{
    compileAndLinkShader();

    glClearColor(0.5f, 0.5f, 0.5f, 1.0f);

    glDisable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glEnable(GL_MULTISAMPLE);
}

void SceneModelTess::update(float t)
{
    // Update frame time every 0.5 seconds
    if (t - toSecond >= 0.5f)
    {
        m_uiData.frameTime = (t - lastTime) * 1000;
        toSecond = t;
    }

    lastTime = t;
}

void SceneModelTess::render()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    vec3 cameraPos(0.0f, 0.0f, 2.0f);
    view = glm::lookAt(cameraPos, vec3(0.0f, 0.0f, 0.0f), vec3(0.0f, 1.0f, 0.0f));

    model = mat4(1.0f);
    model = glm::translate(model, translationVec);
    model = glm::translate(model, vec3(0.0f, -2.0f, 0.0f));

    if (m_uiData.tessEnabled)
    {
        progTess.use();

        setUniforms(progTess);

        loadedModel->renderWithTess();
    }
    else
    {
        prog.use();

        setUniforms(prog);

        loadedModel->render();
    }
        

    glFinish();
}

void SceneModelTess::renderGUI()
{
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();
    ImGui::Begin("Options - Tessellation 3D Triangle Model");
    ImGui::Spacing();
    ImGui::Text("FrameTime [ms]: %.5f", m_uiData.frameTime);
    ImGui::Text("Frames    [s]: %i", int(1000.0f / m_uiData.frameTime));
    ImGui::Spacing();
    ImGui::Checkbox("Show Wireframe", &m_uiData.wireEnabled);
    ImGui::Checkbox("Use Tessellation", &m_uiData.tessEnabled);
    ImGui::Spacing();
    if (ImGui::CollapsingHeader("Uniform Tessellation"))
    {
        ImGui::SliderInt("Inner Level[0]", &m_uiData.tessInner0, 1, 64);
        ImGui::SliderInt("Outer Level[0]", &m_uiData.tessOuter0, 1, 64);
        ImGui::SliderInt("Outer Level[1]", &m_uiData.tessOuter1, 1, 64);
        ImGui::SliderInt("Outer Level[2]", &m_uiData.tessOuter2, 1, 64);
    }
    ImGui::Spacing();
    if (ImGui::CollapsingHeader("Depth-based Tessellation"))
    {
        ImGui::Checkbox("Enable depth-based", &m_uiData.depthEnabled);
        ImGui::SliderInt("Min Tessellation Level", &m_uiData.minTessLevel, 1, 64);
        ImGui::SliderInt("Max Tessellation Level", &m_uiData.maxTessLevel, 1, 64);
        ImGui::SliderFloat("Min Depth", &m_uiData.minDepth, 1.0f, 50.0f);
        ImGui::SliderFloat("Max Depth", &m_uiData.maxDepth, 1.0f, 50.0f);
    }

    ImGui::End();
    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}

void SceneModelTess::setUniforms(GLSLProgram& prog)
{
    mat4 mv = view * model;
    prog.setUniform("ModelViewMatrix", mv);
    prog.setUniform("NormalMatrix", mat3(vec3(mv[0]), vec3(mv[1]), vec3(mv[2])));
    prog.setUniform("MVP", projection * mv);
    prog.setUniform("ViewportMatrix", viewport);

    prog.setUniform("LightDirection", vec3(0.0f, 0.0f, 1.0f));
    prog.setUniform("Ka", 0.1f, 0.0f, 0.0f);
    prog.setUniform("Kd", 1.0f, 0.0f, 0.0f);
    prog.setUniform("Ks", 1.0f, 1.0f, 1.0f);
    prog.setUniform("SpecExp", 128.0f);
    prog.setUniform("LineWidth", 0.8f);
    prog.setUniform("LineColor", vec4(1.0f, 1.0f, 1.0f, 1.0f));

    prog.setUniform("Inner0", m_uiData.tessInner0);
    prog.setUniform("Outer0", m_uiData.tessOuter0);
    prog.setUniform("Outer1", m_uiData.tessOuter1);
    prog.setUniform("Outer2", m_uiData.tessOuter2);

    prog.setUniform("WireEnabled", m_uiData.wireEnabled);
    prog.setUniform("TessEnabled", m_uiData.tessEnabled);
    prog.setUniform("DepthEnabled", m_uiData.depthEnabled);
    prog.setUniform("MinTessLevel", m_uiData.minTessLevel);
    prog.setUniform("MaxTessLevel", m_uiData.maxTessLevel);
    prog.setUniform("MaxDepth", m_uiData.maxDepth);
    prog.setUniform("MinDepth", m_uiData.minDepth);
}

void SceneModelTess::resize(int w, int h)
{
    glViewport(0, 0, w, h);

    float w2 = w / 2.0f;
    float h2 = h / 2.0f;
    viewport = mat4(vec4(w2, 0.0f, 0.0f, 0.0f),
        vec4(0.0f, h2, 0.0f, 0.0f),
        vec4(0.0f, 0.0f, 1.0f, 0.0f),
        vec4(w2 + 0, h2 + 0, 0.0f, 1.0f));
    projection = glm::perspective(glm::radians(60.0f), (float)w / h, 0.3f, 100.0f);
}

void SceneModelTess::translationUpdate(glm::vec3 transVec)
{
    // Updates translation vector based on keyboard input
    translationVec += transVec;
}

void SceneModelTess::compileAndLinkShader()
{
    try {
        progTess.compileShader("shader/modeltess.vs.glsl", GLSLShader::VERTEX);
        progTess.compileShader("shader/modeltess.fs.glsl", GLSLShader::FRAGMENT);
        progTess.compileShader("shader/modeltess.gs.glsl", GLSLShader::GEOMETRY);
        progTess.compileShader("shader/modeltess.tes.glsl", GLSLShader::TESS_EVALUATION);
        progTess.compileShader("shader/modeltess.tcs.glsl", GLSLShader::TESS_CONTROL);
        progTess.link();

        prog.compileShader("shader/modeltess.vs.glsl", GLSLShader::VERTEX);
        prog.compileShader("shader/modeltess.fs.glsl", GLSLShader::FRAGMENT);
        prog.compileShader("shader/modeltess.gs.glsl", GLSLShader::GEOMETRY);
        prog.link();
        prog.use();
    }
    catch (GLSLProgramException& e) {
        cerr << e.what() << endl;
        exit(EXIT_FAILURE);
    }
}

