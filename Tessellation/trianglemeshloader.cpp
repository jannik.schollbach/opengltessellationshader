#include <fstream>
#include <string>
#include "trianglemeshloader.h"

using namespace glm;

TriangleMeshLoader::TriangleMeshLoader(const std::string& objFile= "./resources/egyptian.obj") :
	nVerts(0),
	vao(0)
{
	loadMeshFromFile(objFile);
}

TriangleMeshLoader::~TriangleMeshLoader() {
	deleteBuffers();
}

void TriangleMeshLoader::deleteBuffers() {
	if (buffers.size() > 0) {
		glDeleteBuffers((GLsizei)buffers.size(), buffers.data());
		buffers.clear();
	}

	if (vao != 0) {
		glDeleteVertexArrays(1, &vao);
		vao = 0;
	}
}

void TriangleMeshLoader::initBuffers(std::vector<i32vec3>& indices, std::vector<f32vec3>& points, std::vector<f32vec3>& normals, std::vector<f32vec2>& texCoords) 
{
	if (!buffers.empty()) deleteBuffers();

	// Must have data for indices, points, and normals
	if (indices.data() == nullptr || points.data() == nullptr || normals.data() == nullptr)
		return;

	nVerts = (GLuint)indices.size() * 3;

	GLuint indexBuf = 0, posBuf = 0, normBuf = 0, tcBuf = 0, tangentBuf = 0;
	glGenBuffers(1, &indexBuf);
	buffers.push_back(indexBuf);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuf);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(i32vec3), indices.data(), GL_STATIC_DRAW);

	glGenBuffers(1, &posBuf);
	buffers.push_back(posBuf);
	glBindBuffer(GL_ARRAY_BUFFER, posBuf);
	glBufferData(GL_ARRAY_BUFFER, points.size() * sizeof(f32vec3), points.data(), GL_STATIC_DRAW);

	glGenBuffers(1, &normBuf);
	buffers.push_back(normBuf);
	glBindBuffer(GL_ARRAY_BUFFER, normBuf);
	glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(f32vec3), normals.data(), GL_STATIC_DRAW);

	if (texCoords.data() != nullptr) {
		glGenBuffers(1, &tcBuf);
		buffers.push_back(tcBuf);
		glBindBuffer(GL_ARRAY_BUFFER, tcBuf);
		glBufferData(GL_ARRAY_BUFFER, texCoords.size() * sizeof(f32vec2), texCoords.data(), GL_STATIC_DRAW);
	}

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuf);

	// Position
	glBindBuffer(GL_ARRAY_BUFFER, posBuf);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);  // Vertex position

	// Normal
	glBindBuffer(GL_ARRAY_BUFFER, normBuf);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(1);  // Normal

	// Tex coords
	if (texCoords.data() != nullptr) {
		glBindBuffer(GL_ARRAY_BUFFER, tcBuf);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(2);  // Tex coord
	}

	glBindVertexArray(0);
}

void TriangleMeshLoader::loadMeshFromFile(const std::string& file)
{
	std::vector<f32vec3> m_vertices;
	std::vector<i32vec3> m_indices;
	std::vector<f32vec3> m_normals;
	std::vector<f32vec2> m_texCoords;

	Assimp::Importer importer;

	auto scene = importer.ReadFile(file, aiProcess_CalcTangentSpace |
		aiProcess_Triangulate |
		aiProcess_JoinIdenticalVertices |
		aiProcess_SortByPType);

	if (!scene)
	{
		// If the import failed, report it
		std::cerr << importer.GetErrorString() << std::endl;

		return;
	}
	else
	{
		std::cout << "Imported " << scene->mMeshes[0]->mName.C_Str() << " successfully!" << std::endl;
		if (scene->HasMeshes())
		{
			//std::cout << "Scene has " << scene.mNumMeshes << " Meshes ...!" << std::endl;

			// Get Mesh Objects of Scene
			auto mesh = scene->mMeshes;
			auto node = scene->mRootNode;

			//std::cout << "Scene root node has " << node.mNumMeshes << " meshes." << std::endl;
			//std::cout << "Scene root node has " << node.mNumChildren << " children." << std::endl;

			// Iterate all meshes of the first node
			for (int i = 0; i < node->mNumMeshes; i++)
			{
				//std::cout << "Mesh " << i << std::endl;

				//Reading all Vertex Data
				for (int j = 0; j < (mesh[i]->mNumVertices); j++)
				{
					f32vec3 v(mesh[i]->mVertices[j].x, mesh[i]->mVertices[j].y, mesh[i]->mVertices[j].z);
					m_vertices.push_back(v);

					if (mesh[i]->HasNormals())
					{
						f32vec3 n(mesh[i]->mNormals[j].x, mesh[i]->mNormals[j].y, mesh[i]->mNormals[j].z);
						m_normals.push_back(n);
					}

					if (mesh[i]->HasTextureCoords(0))
					{
						f32vec2 n(mesh[i]->mTextureCoords[0][j].x, mesh[i]->mTextureCoords[0][j].y);
						m_texCoords.push_back(n);
					}
					else
						m_texCoords.push_back(f32vec2(0.0f, 0.0f));
				}

				//Reading all Index Data
				if (mesh[i]->HasFaces())
				{
					for (int j = 0; j < mesh[i]->mNumFaces; j++)
					{
						glm::i32vec3 ind;
						ind.x = mesh[i]->mFaces[j].mIndices[0];
						ind.y = mesh[i]->mFaces[j].mIndices[1];
						ind.z = mesh[i]->mFaces[j].mIndices[2];
						m_indices.push_back(ind);
					}
				}
			}

			if (node->mNumChildren > 0)
			{
				for (int i = 0; i < node->mNumChildren; i++)
				{
					//std::cout << "Mesh " << i << std::endl;

					//Reading all Vertex Data
					for (int j = 0; j < (mesh[i]->mNumVertices); j++)
					{
						f32vec3 v(mesh[i]->mVertices[j].x, mesh[i]->mVertices[j].y, mesh[i]->mVertices[j].z);
						m_vertices.push_back(v);

						if (mesh[i]->HasNormals())
						{
							f32vec3 n(mesh[i]->mNormals[j].x, mesh[i]->mNormals[j].y, mesh[i]->mNormals[j].z);
							m_normals.push_back(n);
						}

						if (mesh[i]->HasTextureCoords(0))
						{
							f32vec2 n(mesh[i]->mTextureCoords[0][j].x, mesh[i]->mTextureCoords[0][j].y);
							m_texCoords.push_back(n);
						}
						else
							m_texCoords.push_back(f32vec2(0.0f, 0.0f));
					}

					//Reading all Index Data
					if (mesh[i]->HasFaces())
					{
						for (int j = 0; j < mesh[i]->mNumFaces; j++)
						{
							glm::i32vec3 ind;
							ind.x = mesh[i]->mFaces[j].mIndices[0];
							ind.y = mesh[i]->mFaces[j].mIndices[1];
							ind.z = mesh[i]->mFaces[j].mIndices[2];
							m_indices.push_back(ind);
						}
					}
				}
			}

			//Sanity Check
			if (m_vertices.size() == mesh[0]->mNumVertices)
			{
				std::cout << "Loaded all " << m_vertices.size() << " Vertices from ASSIMP-MESH to Vector" << std::endl;
			}
			if (m_indices.size() == mesh[0]->mNumFaces)
			{
				std::cout << "Loaded all " << m_indices.size() << " Indices from ASSIMP-MESH to Vector" << std::endl;
			}
			if (m_normals.size() == mesh[0]->mNumVertices)
			{
				std::cout << "Loaded all " << m_normals.size() << " Normals from ASSIMP-MESH to Vector" << std::endl;
			}
			if (m_texCoords.size() == mesh[0]->mNumVertices)
			{
				std::cout << "Loaded all " << m_texCoords.size() << " Texture Coords from ASSIMP-MESH to Vector" << std::endl;
			}

			std::cout << std::endl;

			// Initialize buffers with loaded model
			initBuffers(m_indices, m_vertices, m_normals, m_texCoords);
		}
		else
		{
			return;
		}
	}
}

void TriangleMeshLoader::render() const {
	if (vao == 0) return;

	glBindVertexArray(vao);
	glDrawElements(GL_TRIANGLES, nVerts, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
}

void TriangleMeshLoader::renderWithTess() const {
	if (vao == 0) return;

	glPatchParameteri(GL_PATCH_VERTICES, 3);
	glBindVertexArray(vao);
	glDrawElements(GL_PATCHES, nVerts, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
}