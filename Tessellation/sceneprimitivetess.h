#ifndef SCENEPRIMITIVETESS_H
#define SCENEPRIMITIVETESS_H

#include "scene.h"
#include "glslprogram.h"
#include "cookbookogl.h"

#include <glm/glm.hpp>

class ScenePrimitiveTess : public Scene
{
private:
    GLSLProgram progSolid;
    GLSLProgram progIsoline;
    GLSLProgram progTriangle;
    GLSLProgram progQuad;

    GLuint vaoHandleIsoline;
    GLuint vaoHandleTriangle;
    GLuint vaoHandleQuad;

    glm::mat4 viewport;

    void setMatrices(GLSLProgram& prog);
    void compileAndLinkShader();

    /**
      Tessellation parameters.
      */
    struct
    {
        bool useTessIsoline = false;
        bool useTessTriangle = false;
        bool useTessQuad = true;

        int numSegments = 1;
        int numStrips = 1;

        int tessInner0 = 1;
        int tessInner1 = 1;
        int tessOuter0 = 1;
        int tessOuter1 = 1;
        int tessOuter2 = 1;
        int tessOuter3 = 1;

        double   frameTime = 0.0f;
    } m_uiData;

    double toSecond = 0.0f;
    double lastTime = 0.0f;

public:
    ScenePrimitiveTess();

    void initScene();
    void initIsoline();
    void initTriangle();
    void initQuad();
    void update( float t );
    void render();
    void renderGUI();
    void resize(int, int);
    void translationUpdate(glm::vec3 transVec);
};

#endif // SCENEPRIMITIVETESS_H
