#pragma once

#include "cookbookogl.h"

#include <glm/glm.hpp>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <iostream>
#include <vector>

class TriangleMeshLoader 
{
public:
	TriangleMeshLoader(const std::string& objFile);

	~TriangleMeshLoader();

	void render() const; // Draw call for triangle mesh
	void renderWithTess() const; // Draw call with tessellation shaders

private:
	GLuint nVerts;     // Number of vertices
	GLuint vao;        // The Vertex Array Object

	// Vertex buffers
	std::vector<GLuint> buffers;

	void deleteBuffers();

	void initBuffers(std::vector<glm::i32vec3>& indices, std::vector<glm::f32vec3>& points, std::vector<glm::f32vec3>& normals, std::vector<glm::f32vec2>& texCoords);

	void loadMeshFromFile(const std::string& file);
};