# OpenGL Tessellation Shader

This project demonstrates the OpenGL tessellation shader pipeline with examples of basic subdivision algorithms and the generation of primitives approximating curved geometry. 
Based on OpenGL 4 Shading Language Cookbook by David Wolff.
The project is implemented in C++ with OpenGL and the GLSL shading language.
The repository includes all necessary files like libraries, include-directories, .obj-files and shaders. All external dependencies 
are found in their corresponding folder in the repository.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 
See Installing and running the project for notes on how to deploy the project on a live system.

### Installing and running the project

For developing and testing purposes simply run the Polygon_to_Point.sln file with Microsoft Visual Studio 2019.
Windows SDK Version **10.0.17763.0** needs to be installed (check general project configuration properties in Visual Studio by right-clicking on the project in the project explorer).
Sometimes you need to update or change the Platform Toolset to **v142** within the project configuration properties by right-clicking on the project in the project explorer.
If you can run the project without doing any of these changes, no further steps need to be done.
All libraries and external plug-ins can be used right away.

All libraries are linked statically, so that the program can be executed without 
the need for additional installation of dynamic libraries on the system.

### Project configuration

* IDE: Visual Studio 2019
* Windows SDK Version: 10.0.17763.0
* Plattform Toolset: Visual Studio 2019 (v142)

### Include-directories

* [Assimp] 	(https://www.assimp.org/index.php/downloads) - loading 3D models
* [GLM]    	(https://glm.g-truc.net/) - C++ mathematics library
* [GLFW]   	(https://www.glfw.org/download.html) - OpenGL window management
* [OpenGL] 	(https://www.opengl.org/) - OpenGL graphics framework
* [GLAD]   	(https://github.com/Dav1dde/glad) - Multi-Language GL/GLES/EGL/GLX/WGL Loader-Generator based on the official specs.
* [ImGUI]  	(https://github.com/ocornut/imgui) - Graphical user interface for C++

### Linking the project

The Visual Studio Solution is configured to include and link against all dependencies.
The dependencies can be found in their corresponding folders in the repository (include, libs).

## User Guide

The rendered scene can be controlled by keyboard and mouse input. The GUI offers different options for debugging and demonstration purposes for switching between different tessellation techniques. 
Arguments:

    * "tess-basics", "Demonstrates how tessellation works on different primitives"
    * "tess-teapot", "Uses tessellation to draw teapots, varies the amount of tessellation with depth"
    * "tess-model", "Loads a model mesh via assimp to demonstrate tessellation on a 3D triangle mesh"


[![tess-overview.png](https://i.postimg.cc/xC2RFSSd/tess-overview.png)](https://postimg.cc/p9szp4rN)

## Authors

* **Jannik Schollbach** - schollbachja81835@th-nuernberg.de


